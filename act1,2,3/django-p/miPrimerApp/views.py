from django.shortcuts import render, HttpResponse
from django.db.models import Q
from .models import *
# Create your views here.
def index(request):

    varEstudiante = Estudiante.objects.filter(grupo=1)
    varEst = Estudiante.objects.filter(grupo=4)
    varEs = Estudiante.objects.filter(Q(apellidos='Gómez') | Q(apellidos="Mendoza") | Q(apellidos="Landaverde") | Q(apellidos="Garces"))
    varEs1 = Estudiante.objects.exclude(Q(edad= 19)| Q(edad=25))
    varEs2 = Estudiante.objects.filter(Q(grupo= 3) & Q(edad=22))
    varEs3 = Estudiante.objects.all();
    return render(request,'index.html',{'varEstudiante':varEstudiante, 'varEst':varEst, 'varEs':varEs,'varEs1':varEs1,'varEs2':varEs2,'varEs3':varEs3})
